package geometry;

public class Cylindre extends Geometry3D {

    double h;

    Cylindre(double xCenter, double yCenter, String name, double r, double h) {
        super(xCenter, yCenter, name, r);
        this.h = h;
    }

    @Override
    public void draw() {

    }

    @Override
    public double area() {
        return PI * 2 * r * h;
    }
}