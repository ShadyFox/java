package geometry;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

public class Appli {
    public static void main(String[] args) {

        ArrayList<Geometry2D> list = new ArrayList<Geometry2D>();

        Geometry2D g1 = new Square(0, 0, 2, 2, "G1");
        Geometry2D g2 = new Square(0, 0, 3, 3, "G2");
        Geometry2D g3 = new Square(0, 0, 4, 4, "G3");

        list.add(g1);
        list.add(g2);
        boolean add = list.add(g3);

        for(Geometry2D element : list){
            System.out.println(element.area() + " " + element.name);
        }

        Collections.sort(list, new Comparator<Geometry2D>(){
                    public int compare(Geometry2D s, Geometry2D t) {
                        if (s.area() == t.area()) return 0;
                        if (s.area() > t.area()) return -1;
                        return 1;
                    }});

        for(Geometry2D element : list){
            System.out.println(element.area() + " " + element.name);
        }
    }

}