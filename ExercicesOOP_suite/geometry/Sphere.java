package geometry;

public class Sphere extends Geometry3D {

    Sphere(double xCenter, double yCenter, String name, double r) {
        super(xCenter, yCenter, name, r);
    }

    @Override
    public void draw() {

    }

    @Override
    public double area() {
        return PI * 4 * r * r;
    }
}
