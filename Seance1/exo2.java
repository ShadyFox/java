package exo2;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

public class exo2 {
    public static void main(String[] args) {
        Instant start = Instant.now();
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Integer> gradeList = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Sha\\IdeaProjects\\ExoSeance2\\src\\exo2\\file"));
            String line = reader.readLine();

            while (line != null) {
                String name = line.split(":")[0];
                String grade = line.split(":")[1];
                nameList.add(name);
                gradeList.add(Integer.parseInt(grade));
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        int sum = 0;
        for (int grade: gradeList) {
            sum += grade;
        }
        System.out.println("Average grade : " + sum / gradeList.size());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a mail address : ");
        String userName = scanner.next();
        scanner.close();

        boolean found = false;

        for (int i = 0; i < nameList.size(); i += 1) {
            if (userName.equals(nameList.get(i))) {
                found = true;
                System.out.println("Grade of " + userName + " is " + gradeList.get(i) + ".");
                break;
            }
        }

        if (!found) {
            System.out.println("Can't find " + userName);
        }

        Duration duration = Duration.between(start, Instant.now());
        System.out.println("Execution time : " + duration.toMillis());
    }
}
