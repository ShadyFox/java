package exercice2;

public class Mark implements Cloneable{
    private float value;
    private String subject;
    public Mark() {
    }
    public Mark(float value, String subject) {
        if (value>=0 && value<=20)
            this.value = value;
        this. subject = subject;
    }
    public float getValue() {
        return value;
    }
    public void setValue(float value) {
        if (value>=0 && value<=20)
            this.value = value;
    }
    public String getSubject () {
        return subject;
    }
    public void setSubject (String subject) {
        this. subject = subject;
    }

    public Object clone() {
        Object o = null;
        try {
            o = super.clone();
        } catch(CloneNotSupportedException cnse) {
            cnse.printStackTrace(System.err);
        }
        return o;
    }

    public String toString() {
        return "Value " + this.getValue() + " Subject : " + this.getSubject();
    }

}