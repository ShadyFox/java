package geometry;

public abstract class Geometry3D implements Geometry{

    double xCenter;
    double yCenter;
    String name;
    double r;

    Geometry3D(double xCenter, double yCenter, String name, double r){
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        this.name = name;
        this.r = r;
    }
}