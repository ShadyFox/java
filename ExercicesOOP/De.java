package exercice1;
import java.util.Random;

public class De {

    int id;
    int number;

    De(int id){
        this.id = id;
    }

    void launch(){
        Random r = new Random();
        this.number = r.nextInt(6) + 1;
    }

    void displayValue(){
        System.out.println("Value of id : " + id + " is : " + this.number);
    }

    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public static void main(String[] args) {
        /* Part 1
        De de1 = new De(1);
        de1.launch();
        de1.displayValue();
        */

        /*Part 2
        for(int i = 0; i < Integer.parseInt(args[0]); i++) {
            De de1 = new De(1);
            de1.launch();
            de1.displayValue();
        }
        */
        try {

            //Part 3
            for (int i = 0; i < Integer.parseInt(args[0]); i++) {
                De de1 = new De(1);
                de1.launch();
                de1.displayValue();

                De de12 = new De(2);
                de12.launch();
                de12.displayValue();

                if (de1.getNumber() == de12.getNumber()) {
                    System.out.println("You won !");
                } else {
                    System.out.println("You lose !");
                }
            }
        } catch (Exception e){

            e.printStackTrace();
        }
    }
}
