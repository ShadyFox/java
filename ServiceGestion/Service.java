package ServiceGestion;
import java.util.*;

public class Service {

    protected String name;
    ArrayList<Employe> employes;

    public Service(String name){
        this.name = name;
        this.employes = new ArrayList<Employe>();
    }

    public String getNameService(){
        return this.name;
    }

    protected void hire(Employe emp){
        this.employes.add(emp);
        System.out.println("Le service " + this.getNameService() + " a embauché le salarié " + emp.getName());
    }

    protected void fireEmploye(Employe empToFire) {
        for(Employe emp : this.employes) {
            if(emp.getId() == empToFire.getId()) {
                this.employes.remove(emp);
                System.out.println("L'employé " + empToFire.getName() + " a été viré.");
                return;
            }
        }
        System.out.println("L'employé " + empToFire.getName() + " n'a été pas été trouvé.");
    }
}
