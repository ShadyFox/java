CAS A :

public class Employee {

	int id;
	String name;
	double salary;
	Service service;
	
	Employee(String name, double salary, Service service){
		this.name = name;
		this.salary = salary;
		this.service = service;
	}
	
}

CAS B :

public class Service {
	
	int id;
	String name;
	ArrayList<Employee> employees;
	
	Service(String name){
		this.name = name;
	}
	
}



public class Person {
	
	int id;
	String firstName;
	String lastName;
	Person mother;
	Person father;
	
	Person(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName =lastName;
	}
	
}

CAS C :

public class Student {

	int id;
	String name;
	ArrayList<Course> courses;
	ArrayList<Result> results;
}

public class Course {

	int id;
	String name;
	
}