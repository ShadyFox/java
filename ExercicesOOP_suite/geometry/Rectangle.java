package geometry;

public abstract class Rectangle extends Geometry2D {


    Rectangle(double xCenter, double yCenter, double l, double h, String name) {
        super(xCenter, yCenter, l, h, name);
    }

    double perimeter(){
        return 2*(l+h);
    }


    @Override
    public double area() {
        return l * h;
    }
}
