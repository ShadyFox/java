package ServiceGestion;

public class Application {

    public static void main (String[] args){
        Service service1 = new Service("Ligue des héros");
        Employe employe1 = new Employe("Superman", 2000);

        employe1.joinService(service1);
        service1.hire(employe1);

        Employe employe2 = new Employe("Batman", 2400);
        employe2.joinService(service1);
        service1.hire(employe2);

        service1.fireEmploye(employe1);
        employe1.leaveService();
    }
}
