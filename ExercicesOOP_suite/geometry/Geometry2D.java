package geometry;

public abstract class Geometry2D implements Geometry, Comparable<Geometry2D>{

    double xCenter;
    double yCenter;
    double l;
    double h;
    String name;

    Geometry2D(double xCenter, double yCenter, double l, double h, String name){
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        this.l = l;
        this.h = h;
        this.name = name;
    }

    @Override
    public int compareTo(Geometry2D g) {
        return name.compareTo(g.name);
    }
}