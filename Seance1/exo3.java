package exo3;
import java.lang.*;
import java.util.ArrayList;

public class exo3 {
    public static void main(String[] args) throws Exception {
        try {
            sum2(new String[]{"123", "aAZ", "b123", "AAAAdd555c"});
        } catch (Exception e) {

            System.out.println("Exception : " + e);

        }
    }

    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    private static void sum(String[] strings) throws Exception {
        int sum = 0;

        for (String string : strings) {

            if (!isNumeric(string)) {
                throw new Exception("String " + string + " is (are) not numeric ");
            }
            else{
                sum+= Integer.parseInt(string);
            }
        }

        float average = sum/ strings.length;
        System.out.println("Average : " + average);
    }

    private static void sum2(String[] strings) throws Exception {
        int sum = 0;
        boolean exception = false;
        ArrayList<String> incorrectStrings = new ArrayList<>();

        for (String string : strings) {

            if (!isNumeric(string)) {
                exception = true;

                incorrectStrings.add(string);
            }
            else{
                sum+= Integer.parseInt(string);
            }
        }

        float average = sum/ strings.length;
        System.out.println("Average : " + average);



        if(exception){
            StringBuilder wrongStrings = new StringBuilder();
            for (String current: incorrectStrings) {
                wrongStrings.append(current + ",");
            }
            wrongStrings.setLength(wrongStrings.length() - 1);
            throw new Exception("String " + wrongStrings + " is (are) not numeric ");
        }
    }
}
