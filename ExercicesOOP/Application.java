package exercice2;

import java.util.ArrayList;

public class Application implements Cloneable{

    ArrayList<ArrayList<Mark>> students_marks;

    public Application(){

        ArrayList<Mark> Student1 = new ArrayList<Mark>();
        ArrayList<Mark> Student2 = new ArrayList<Mark>();
        ArrayList<Mark> Student3 = new ArrayList<Mark>();

        students_marks = new ArrayList<ArrayList<Mark>>();

        Student1.add(new Mark(12, "Java"));
        Student1.add(new Mark(14, "BD"));

        Student2.add(new Mark(12, "Java"));
        Student2.add(new Mark(10, "BD"));
        Student2.add(new Mark(15, "Maths"));
        Student2.add(new Mark(13, "Français"));

        Student3.add(new Mark(18, "BD"));

        students_marks.add(Student1);
        students_marks.add(Student2);
        students_marks.add(Student3);
    }

    public void print(){
        int i = 0;
        for (ArrayList<Mark> studentMarks: students_marks) {
            System.out.println("Student " + i + " :");
            i++;
            for (Mark mark: studentMarks) {
                System.out.println("\tSubject : " + mark.getSubject() + " Mark : " + mark.getValue());
            }
        }
    }

    public float average(int position){

        ArrayList<Mark> studentMarks = students_marks.get(position);
        int i = 0;
        float somme = 0;
        for (Mark mark: studentMarks) {
            somme += mark.getValue();
            i++;
        }

        return somme/i;
    }

    public void add(int position, String subject, float value ){

        ArrayList<Mark> studentMarks = students_marks.get(position);
        studentMarks.add(new Mark(value, subject));
    }

    public int compute(){

        int max = 0;
        int tempMax = 0;

        for (ArrayList<Mark> studentMarks: students_marks) {
            for (Mark mark: studentMarks) {
                tempMax++;
            }
            if(tempMax > max) max = tempMax;
            tempMax = 0;
        }

        return max;
    }

    public void averageGlobal(){

        int i = 0;
        for (ArrayList<Mark> studentMarks: students_marks) {
            System.out.println("Student " + i + " : " + this.average(i));
            i++;
        }
    }

    public Object clone() {

        ArrayList<ArrayList<Mark>> clone_students_marks = new ArrayList<ArrayList<Mark>>();

        try {

            int i = 0;
            int j = 0;
            for (ArrayList<Mark> studentMarks: students_marks) {
                ArrayList<Mark> line = new ArrayList<Mark>();

                for (Mark mark: studentMarks) {
                    line.add((Mark) students_marks.get(i).get(j).clone());
                    j++;
                }
                clone_students_marks.add(line);
                j = 0;
                i ++;
            }

        } catch(Exception cnse) {
            cnse.printStackTrace(System.err);
        }

        return clone_students_marks;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder();

        int i = 0;
        for (ArrayList<Mark> studentMarks: students_marks) {
            builder.append("Student " + i + " :\n");
            i++;
            for (Mark mark: studentMarks) {
                builder.append("\tSubject : " + mark.getSubject() + " Mark : " + mark.getValue());
            }
        }

        return builder.toString();
    }

    public static void main (String[] args){

        Application app = new Application();
        app.print();
        System.out.println(app.average(2));
        app.add(2, "FR", 20);
        System.out.println(app.average(2));
        System.out.println(app.compute());
        app.averageGlobal();
    }
}