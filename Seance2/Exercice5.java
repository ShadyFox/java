package Exercice5;

public class Exercice5 {

    static boolean equals(String word1, String word2, int mistakesAllowed) {
        if(word1.equals(word2))
            return true;

        if(word1.length() == word2.length()) {
            for(int i = 0; i < word1.length(); i++) {
                if(word1.charAt(i) != word2.charAt(i)) {
                    mistakesAllowed--;
                    if(mistakesAllowed < 0) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public static void main(String[] args) {

        String[] words = { "aas", "ace", "alu", "are", "api", "ays", "bec",
                "bel", "bey", "ben", "boy", "deb", "des", "dom",
                "dot", "daw", "fax", "fan", "faq", "fob", "hem",
                "hop", "man", "mao", "mug","mus", "mie", "sur",
                "dey", "mur"
        };

        System.out.println("Voisins 'bel' :");
        for (String s: words) {
            if(equals("bel", s, 1)){

                System.out.println(s);
            }
        }
        System.out.println("Voisins 'dey' :");
        for (String s: words) {
            if(equals("dey", s, 1)){

                System.out.println(s);
            }
        }
    }
}
