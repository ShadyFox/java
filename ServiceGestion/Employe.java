package ServiceGestion;
import java.util.concurrent.atomic.AtomicInteger;

public class Employe {

    protected String name;
    protected double salary;
    private static final AtomicInteger count = new AtomicInteger(0);
    protected int id;
    protected Service service;

    public Employe(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.id = count.incrementAndGet();
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public double getId() {
        return id;
    }

    public String toString() {
        return "Id : " + id + " - Name's employe : " + getName() + " - Salary : " + getSalary();
    }

    protected void leaveService(){
        String servName = this.service.getNameService();
        this.service = null;
        System.out.println("J'ai (" + this.getName() + ") quitté le service " + servName);
    }

    protected void joinService(Service service){
        this.service = service;
        System.out.println("J'ai (" + this.getName() + ") rejoint le service " + service.getNameService());
    }

    public String getServiceName(){
        if(this.service != null){
            return this.service.getNameService();
        }
        else{
            return "No service.";
        }
    }
}