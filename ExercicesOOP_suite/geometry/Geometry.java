package geometry;

public interface Geometry {

    void draw();
    double area();
    Double PI = 3.14;

}