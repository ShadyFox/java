package geometry;

public abstract class Ellipsis extends Geometry2D {


    Ellipsis(double xCenter, double yCenter, double l, double h, String name) {
        super(xCenter, yCenter, l, h, name);
    }
}
